import { createElement } from '../helpers/domHelper';
import { fighterService } from '../services/fightersService'
import { fighters } from '../helpers/mockData';


export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  if (fighter) {
    fighterElement.append(getFighterInfo(fighter, positionClassName));
  }
  // todo: show fighter info (image, name, health, etc.)
  return fighterElement;
}

function getFighterInfo(fighter, positionClassName) {
  const info = createElement({
    tagName: 'div',
    className: 'fighter-preview___info'
  });

  const image = createFighterImage(fighter);
  const nameElement = createElement({
    tagName: 'h2',
    className: `fighter-preview___root ${positionClassName} info-name`
  });
  nameElement.innerText = fighter.name;
  const descriptionElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName} info-description`
  });
  descriptionElement.innerText = `❤️: ${fighter.health} \n ⚔️: ${fighter.attack} \n 🛡️: ${fighter.defense}`;
  info.append(nameElement, image, descriptionElement);
  return info;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
