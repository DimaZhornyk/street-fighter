import { controls } from '../../constants/controls';


export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {

    var firstFighterMap = {
      ...firstFighter,
      position: 'left',
      currentHealth: firstFighter.health,
      critCooldown: new Date(0),
      setCooldown() {
        this.critCooldown = new Date();
      }
    };
    var secondFighterMap = {
      ...secondFighter,
      position: 'right',
      currentHealth: secondFighter.health,
      critCooldown: new Date(0),
      setCooldown() {
        this.critCooldown = new Date();
      }
    };


    const pressedKeys = [];

    document.addEventListener('keydown', (e) => {
      pressedKeys.push(e.code);
      makeFightAction(firstFighterMap, secondFighterMap, pressedKeys);
      if (firstFighterMap.currentHealth <= 0 || secondFighterMap.currentHealth <= 0) {
        const winner = firstFighterMap.currentHealth <= 0 ? secondFighter : firstFighter;
        resolve(winner);
      }

    });

    document.addEventListener('keyup', (e) => {
      while (pressedKeys.includes(e.code)) {
        let index = pressedKeys.indexOf(e.code);
        if (index > -1) {
          pressedKeys.splice(index, 1);
        }
      }
    });
  });
}

function makeFightAction(fighter1, fighter2, keysMap) {
  const leftHealthIndicator = document.getElementById('left-fighter-indicator');
  const rightHealthIndicator = document.getElementById('right-fighter-indicator');

  switch (true) {
    case controls.PlayerOneCriticalHitCombination.every(btn => keysMap.includes(btn)): {
      processFighterCriticalAttack(fighter1, fighter2, rightHealthIndicator);
    }; break;
    case controls.PlayerTwoCriticalHitCombination.every(btn => keysMap.includes(btn)): {
      processFighterCriticalAttack(fighter2, fighter1, leftHealthIndicator);
    }; break;
    case keysMap.includes(controls.PlayerOneAttack): {
      processFighterAttack(fighter1, fighter2, rightHealthIndicator, keysMap);
    }; break;
    case keysMap.includes(controls.PlayerTwoAttack): {
      processFighterAttack(fighter2, fighter1, leftHealthIndicator, keysMap);
    }; break;
  }
}

function processFighterAttack(attacker, defender, defenderHealthIndicator, keysMap) {
  if (attacker.position == 'left' && keysMap.includes(controls.PlayerOneBlock)) return;
  if (attacker.position == 'right' && keysMap.includes(controls.PlayerTwoBlock)) return;

  defender.currentHealth -= getDamage(attacker, defender, keysMap);
  updateIndicator(defender, defenderHealthIndicator);
}

function processFighterCriticalAttack(attacker, defender, defenderHealthIndicator) {
  if (isCriticalHitOnCooldown(attacker)) return;
  attacker.setCooldown();
  defender.currentHealth -= attacker.attack * 2;

  updateIndicator(defender, defenderHealthIndicator);
}

function isCriticalHitOnCooldown(attacker) {
  const secondsSinceLastUsage = (new Date().getTime() - attacker.critCooldown.getTime()) / 1000;
  return secondsSinceLastUsage < 10;
}

export function getDamage(attacker, defender, keysMap) {
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender, keysMap);
  let damage = hitPower - blockPower;
  damage = damage > 0 ? damage : 0;
  return damage;
}

export function getHitPower(fighter) {
  const critChance = Math.random() + 1;
  return fighter.attack * critChance;
}

export function getBlockPower(fighter, keysMap) {
  // const blockChance = Math.random() + 1;
  // return fighter.defense * blockChance;
  if ((fighter.position == 'left' && keysMap.includes(controls.PlayerOneBlock)) || (fighter.position == 'right' && keysMap.includes(controls.PlayerTwoBlock))) {
    const blockChance = Math.random() + 1;
    return fighter.defense * blockChance;
  } else return 0;
}

export function getCriticalDamage(attacker) {
  return attacker.attack * 2;
}

function updateIndicator(defender, indicator) {
  const indicatorWidth = Math.max(0, (defender.currentHealth * 100) / defender.health);
  indicator.style.width = `${indicatorWidth}%`;
}
