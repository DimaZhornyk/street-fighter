import {showModal} from './modal'
import { createFighterImage } from '../fighterPreview';
import '../../../styles/styles.css';
import App from '../../app';

export function showWinnerModal(fighter) {
  const imageElement = createFighterImage(fighter);
  const modalElement = {
    title: `${fighter.name.toUpperCase()} WINS!`,
    bodyElement: imageElement
  };
  modalElement.onClose = function(){
    const root = document.getElementById('root');
    root.innerHTML = '';
    new App();
  }
  
  showModal(modalElement);  
}
